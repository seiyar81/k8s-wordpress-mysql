# k8s Wordpress Mysql


## Getting started

Just launch the _install_stack.sh_ script and it will create secrets, configmaps, services and deployments for both Wordpress and MySQL.

Then it uses _kubectl port-forward_ to make Wordpress accessible through [http://127.0.0.1:8080](http://127.0.0.1:8080)
