#!/bin/bash

# Technical exercise for Ingenico

ROOT_PATH=`pwd`

# Namespace deletion before starting
kubectl delete ns wordpress

# Namespace creation
kubectl create ns wordpress

# Call mysql deploy script
cd $ROOT_PATH/mysql && sh deploy.sh

kubectl rollout status deployment mysql --namespace wordpress

# Call wordpress deploy script
cd $ROOT_PATH/wordpress && sh deploy.sh

kubectl rollout status deployment wordpress --namespace wordpress

cd $ROOT_PATH

# port forwarding to check if wordpress is reponding well and configured correctly
echo "Open http://127.0.0.1:8080"
kubectl port-forward --namespace wordpress service/wordpress 8080:80