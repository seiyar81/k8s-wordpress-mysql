#!/bin/bash

echo "Deploying mysql"

# Install config
kubectl apply -f configmap.yaml

# Install secret
kubectl apply -f secret.yaml

# Install service
kubectl apply -f service.yaml

# Install deployment
kubectl apply -f deployment.yaml