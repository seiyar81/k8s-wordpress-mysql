#!/bin/bash

echo "Deploying wordpress"

# Install config
kubectl apply -f configmap.yaml

# Install service
kubectl apply -f service.yaml

# Install deployment
kubectl apply -f deployment.yaml